/**
 * MagazineView view component.
 * @module copy from components/theme/View/NewsItemView
 */

import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { Container, Image } from 'semantic-ui-react';

import { flattenToAppURL } from '@plone/volto/helpers';

/**
 * MagazineView view component class.
 * @function MagazineView
 * @params {object} content Content object.
 * @returns {string} Markup of the component.
 */
const MagazineView = ({ content }) => (
  <Container className="view-wrapper">
    <Helmet title={content.title} />
    {content.title && (
      <h1 className="documentFirstHeading">
        {content.title}
        {content.subtitle && ` - ${content.subtitle}`}
      </h1>
    )}
    <p className="test-page">Sample show `p` tag</p>
    {content.description && (
      <p className="documentDescription">{content.description}</p>
    )}
    {content.image && (
      <Image
        className="documentImage"
        alt={content.title}
        title={content.title}
        src={
          content.image['content-type'] === 'image/svg+xml'
            ? flattenToAppURL(content.image.download)
            : flattenToAppURL(content.image.scales.mini.download)
        }
        floated="right"
      />
    )}
    {content.text && (
      <div dangerouslySetInnerHTML={{ __html: content.text.data }} />
    )}
  </Container>
);

/**
 * Property types.
 * @property {Object} propTypes Property types.
 * @static
 */
MagazineView.propTypes = {
  content: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    text: PropTypes.shape({
      data: PropTypes.string,
    }),
  }).isRequired,
};

export default MagazineView;
