/**
 * Add your components here.
 * @module components
 * @example
 * import Footer from './Footer/Footer';
 *
 * export {
 *   Footer,
 * };
 */

export MagazineView from './theme/View/MagazineView';

import EditSummaryBlock from './manage/Blocks/Summary/Edit';
import ViewSummaryBlock from './manage/Blocks/Summary/View';
import EditMainImageBlock from './manage/Blocks/MainImage/Edit';
import ViewMainImageBlock from './manage/Blocks/MainImage/View';
import descriptionSVG from '@plone/volto/icons/description.svg';
import cameraSVG from '@plone/volto/icons/camera.svg';

export const customBlocks = {
  summary: {
    id: 'summary', // The name (id) of the block
    title: 'Summary', // The display name of the block
    icon: descriptionSVG, // The icon used in the block chooser
    group: 'common', // The group (blocks can be grouped, displayed in the chooser)
    view: ViewSummaryBlock, // The view mode component
    edit: EditSummaryBlock, // The edit mode component
    restricted: false, // If the block is restricted, it won't show in the chooser
    mostUsed: true, // A meta group `most used`, appearing at the top of the chooser
    blockHasOwnFocusManagement: false, // Set this to true if the block manages its own focus
    security: {
      addPermission: [], // Future proof (not implemented yet) add user permission role(s)
      view: [], // Future proof (not implemented yet) view user role(s)
    },
  },
  mainimage: {
    id: 'mainimage',
    title: 'Main Image',
    icon: cameraSVG,
    group: 'media',
    view: ViewMainImageBlock,
    edit: EditMainImageBlock,
    restricted: true,
    mostUsed: false,
    security: {
      addPermission: [],
      view: [],
    },
  },
};
