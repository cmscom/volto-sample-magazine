/**
 * Add your config changes here.
 * @module config
 * @example
 * export const settings = {
 *   ...defaultSettings,
 *   port: 4300,
 *   listBlockTypes: {
 *     ...defaultSettings.listBlockTypes,
 *     'my-list-item',
 *   }
 * }
 */

import {
  settings as defaultSettings,
  views as defaultViews,
  widgets as defaultWidgets,
  blocks as defaultBlocks,
} from '@plone/volto/config';

import { customBlocks } from './components';
import {
  // contentTypesViews as defultContentTypesViews,
  layoutViews as defultLayoutViews,
} from '@plone/volto/config/Views';

import { MagazineView } from './components';

const layoutViews = {
  ...defultLayoutViews,
  magazine_view: MagazineView,
};

const modifiedViews = {
  ...defaultViews,
  layoutViews: layoutViews,
};

export const settings = {
  ...defaultSettings,
};

export const views = {
  ...modifiedViews,
};

export const widgets = {
  ...defaultWidgets,
};

const customRequiredBlocks = ['title', 'description', 'mainimage'];

export const blocks = {
  ...defaultBlocks,
  blocksConfig: { ...defaultBlocks.blocksConfig, ...customBlocks },
  requiredBlocks: customRequiredBlocks,
};
